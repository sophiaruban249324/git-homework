<h1>Gitflow vs. trunk-based development</h1>
<h2>Gitflow pros and cons</h2>
<h3>PROS</h3>
<ul>
  <li>Suitable for team with a lot of junior developer</li>
  <li>Suitable for brown-field product</li>
  <li>Working with other vendors/third-party</li>
</ul>
<h3>CONS</h3>
<ul>
  <li>Not suitable for starting-up development</li>
  <li>Reduce development-speed on fast iterations</li>
  <li>Flow visualization looks very complex</li>
</ul>
<h2>Trunk-Based Development pros and cons</h2>
<h3>PROS</h3>
<ul>
  <li>Main branch always in a state ready to be deployed to production</li>
  <li>Suitable for starting-up development</li>
  <li>Support fast iteration development</li>
</ul>

<h3>CONS</h3>
<ul>
  <li>Not suitable for open-source development</li>
  <li>Not suitable for team with a lot of junior developer</li>
  <li>Not suitable when we have established product or manage large teams that implement strict-control due to business needs</li>
</ul>
<h1>Git tag</h1>
<p>Tags are refs that point to specific points in Git history. Use to mark release versions (v1.0, v2.0)</p>
<h1>Git Hooks</h1>
<p>Git hooks are scripts that run automatically every time a particular event occurs in a Git repository</p>
